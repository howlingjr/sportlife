//
//  PlayoffService.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import Alamofire
import ObjectMapper
import RxSwift
import SwiftyJSON


class PlayoffService {
    
    func fetchPlayoff(_ date:String) -> Observable<[Playoff]> {
        return Observable<[Playoff]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.net/data/10s/prod/v1/\(date)/playoffsBracket.json",method: .get)
                //.request("http://data.nba.net/data/10s/prod/v1/2016/playoffsBracket.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<PlayoffResponse>().map(JSON: (json.dictionaryObject)!)!
                        observer.onNext(result.series!)
                        observer.onCompleted()
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
}
*/
