//
//  Playoff.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import ObjectMapper


class PlayoffResponse: Mappable {
    
    var series : Array<Playoff>?
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        series <- map["series"]
        
    }
}

class Playoff: Mappable {
    
    var team1Id : String?
    var team1Wins : String?
    var team2Id : String?
    var team2Wins : String?
    var roundNum : String?
    var confName : String?

    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        team1Id <- map["topRow.teamId"]
        team1Wins <- map["topRow.wins"]
        team2Id <- map["bottomRow.teamId"]
        team2Wins <- map["bottomRow.wins"]
        roundNum <- map["roundNum"]
        confName <- map["confName"]

        
    }
}
*/
