//
//  SnoozeViewModel.swift
//  SportLife
//
//  Created by ludovic grimbert on 22/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import RxSwift


class SnoozeViewModel {
    
    fileprivate let localNotif: LocalNotif
    
    
    var snooze = Variable<Snooze?>(Snooze())
    var sound = Variable<String?>("")
    var modeRepeat = Variable<Bool?>(false)
    
    let notif: Observable<[Snooze]> 
    var info = Variable<String>("")

    init(info: Variable<String>,localNotif:LocalNotif,addItemTap: Observable<Void>){
        self.localNotif = localNotif
        self.info = info
        
        let refreshStream = addItemTap.startWith(())
        /*
        notif = refreshStream
            .flatMapLatest { _ -> Observable<[Snooze]> in
           return localNotif.fetchNotif()
        }
 */
         notif = Observable.combineLatest(self.info.asObservable(), refreshStream.asObservable())
         .flatMapLatest { info -> Observable<[Snooze]> in
            print(info)
         return localNotif.fetchNotif()
         }
        
    }
    
    
    func getDatePicker(date:Date) {
        let calendar = Calendar.current
        self.snooze.value?.hour = calendar.component(.hour, from: date)
        self.snooze.value?.minute = calendar.component(.minute, from: date)
    }
    
    func scheduleSnooze(weekday:Int) {
        var messageRepeat = "main.thisweek".translate
        self.snooze.value?.uuid =  UUID().uuidString
        self.snooze.value?.repeatAlarm = modeRepeat.value
        self.snooze.value?.sound = sound.value
        self.snooze.value?.weekday = weekday
        if (self.snooze.value?.repeatAlarm)! {
            messageRepeat = "main.repeateveryweek".translate
        }
        self.snooze.value?.body = LocalNotif.whichDay(day: weekday, alarmHour: (self.snooze.value?.hour)!, alarmMinute: (self.snooze.value?.minute)!,modeRepeat: messageRepeat)
        
        LocalNotif.newScheduleLocal(snooze: self.snooze.value!)
        
    }
    
    func deleteNotif(uuid:String) {
        LocalNotif.removeNotif(uuid: uuid)
    }
    
    
}
*/
