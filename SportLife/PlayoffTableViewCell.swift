//
//  PlayoffTableViewCell.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit

class PlayoffTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nickname1Label: UILabel!
    @IBOutlet weak var nickname2Label: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var logo1ImageView:UIImageView!
    @IBOutlet weak var logo2ImageView:UIImageView!
    
    
    
    var playoff: Playoff? {
        didSet {
            infoLabel.text = (playoff?.confName)! + "    " + "main.round".translate + " " + (playoff?.roundNum)!
            scoreLabel.text = (playoff?.team1Wins)! + "-" + (playoff?.team2Wins)!
            Logo.displayLogo(nickname: Logo.changeIdToTeam(id: (playoff?.team1Id)!), logoImageView: logo1ImageView)
            Logo.displayLogo(nickname: Logo.changeIdToTeam(id: (playoff?.team2Id)!), logoImageView: logo2ImageView)
            nickname1Label.text = Logo.changeIdToTeam(id: (playoff?.team1Id)!)
            nickname2Label.text = Logo.changeIdToTeam(id: (playoff?.team2Id)!)
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nickname1Label.adjustsFontSizeToFitWidth = true
        nickname1Label.text = ""
        nickname2Label.adjustsFontSizeToFitWidth = true
        nickname2Label.text = ""
        infoLabel.adjustsFontSizeToFitWidth = true
        infoLabel.text = ""
        scoreLabel.adjustsFontSizeToFitWidth = true
        scoreLabel.text = ""
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
*/
