//
//  StandingViewModel.swift
//  SportLife
//
//  Created by ludovic grimbert on 02/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import RxSwift
import Alamofire


class StandingViewModel {
    
    fileprivate let standingService: StandingService
    
   // fileprivate let disposeBag = DisposeBag()
    let standingWestVariable = Variable<[Standing]>([Standing]())
    let standingWest: Observable<[Standing]>
    let standingEastVariable = Variable<[Standing]>([Standing]())
    let standingEast: Observable<[Standing]>
    
    
    init(standingService: StandingService) {
        self.standingService = standingService
        
        // tester avec flapMap à la place de flatMapLatest
        standingWest = standingWestVariable.asObservable()
            .flatMapLatest { _ -> Observable<[Standing]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return standingService.fetchStandingWest(StandingSettings.sharedInstance.getModeApp())
                }
                return Observable.just([Standing]())
        }
        
        
        // tester avec flapMap à la place de flatMapLatest
        standingEast = standingEastVariable.asObservable()
            .flatMapLatest { _ -> Observable<[Standing]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return standingService.fetchStandingEast(StandingSettings.sharedInstance.getModeApp())
                }
                return Observable.just([Standing]())
        }
        
    }
    

}
*/

