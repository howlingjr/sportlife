//
//  DIManager.swift
//  SportLife
//
//  Created by Ludovic Grimbert on 06/02/2022.
//  Copyright © 2022 ludovic grimbert. All rights reserved.
//

import Foundation
import Swinject


final class DIManager {
    
    struct K {
        static var baseUrl = "Constants.Domains.currentBaseURL" // TODO: adresse nba
    }
    
    static func createContainer(router: Router) -> Container {
        
        let container = Container { _ in }

        
        //registerRepository(container)
        registerTabBarItems(container)
        registerTabBar(container, router: router)
        registerModule(container, router: router)
        return container
    }
    
//    static func registerRepository(_ container: Container) {
//        container.register(ADSUserRepository.self) { resolver in
//            UserRepository(withURL: K.baseUrl)
//        }.inObjectScope(.container)
//
//        container.register(ADSEvaluationRepository.self) { resolver in
//            EvaluationRepository(withURL: K.baseUrl)
//        }.inObjectScope(.container)
//
//        container.register(ADSTreatmentsRepository.self) { resolver in
//            TreatmentsRepository(withURL: K.baseUrl)
//        }.inObjectScope(.container)
//    }
  
    static func registerModule(_ container: Container, router: Router) {
//        container.register(OnBoardingModuleProtocol.self) { _ in
//            OnBoardingModuleEntry(router: router, container: container)
//        }.inObjectScope(.container)
//
        container.register(DayModuleProtocol.self) { _ in
            DayModuleEntry(router: router, container: container)
        }.inObjectScope(.container)
//
//        container.register(TestModuleProtocol.self) { _ in
//            TestModuleEntry(router: router, container: container)
//        }.inObjectScope(.container)
//
//        container.register(SettingsModuleProtocol.self) { _ in
//            SettingsModuleEntry(router: router, container: container)
//        }.inObjectScope(.container)
//
//        container.register(ProfilModuleProtocol.self) { _ in
//            ProfilModuleEntry(router: router, container: container)
//        }.inObjectScope(.container)
    }
    
    static func registerTabBar(_ container: Container, router: Router) {
        container.register(NavigationProtocol.self) {  _ in
            let tabbar = TabBarModuleEntry(router: router, container: container)
            return tabbar as NavigationProtocol
        }.inObjectScope(.container)
    }
    
    static func registerTabBarItems(_ container: Container) {
        container.register([NavigationItemProtocol].self) { resolver in
            

            return [NavigationItem(title: TabBarItem.day.title,
                                   image: TabBarItem.day.image,
                                   selectedImage: TabBarItem.day.selectedImage,
                                  module: resolver.resolve(DayModuleProtocol.self).unsafelyUnwrapped)]
        }.inObjectScope(.container)
    }
}
