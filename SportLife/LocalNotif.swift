//
//  LocalNotif.swift
//  SportLife
//
//  Created by ludovic grimbert on 22/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import UserNotifications
import RxSwift


class LocalNotif {
    
    static func whichDay(day:Int,alarmHour:Int,alarmMinute:Int,modeRepeat:String)->String {
        var hour = ""
        var minute = ""
        
        if alarmHour < 10 {
            hour = "0" + "\(alarmHour)"
        }else{
            hour = "\(alarmHour)"
        }
        if alarmMinute < 10 {
            minute = "0" + "\(alarmMinute)"
        }else{
            minute = "\(alarmMinute)"
        }
        
        switch day {
        case 1:
            return modeRepeat + "     " + "main.dimanche".translate + "     " + hour + ":" + minute
        case 2:
            return modeRepeat + "     " + "main.lundi".translate + "     " + hour + ":" + minute
        case 3:
            return modeRepeat + "     " + "main.mardi".translate + "     " + hour + ":" + minute
        case 4:
            return modeRepeat + "     " + "main.mercredi".translate + "     " + hour + ":" + minute
        case 5:
            return modeRepeat + "     " + "main.jeudi".translate + "     " + hour + ":" + minute
        case 6:
            return modeRepeat + "     " + "main.vendredi".translate + "     " + hour + ":" + minute
        case 7:
            return modeRepeat + "     " + "main.samedi".translate + "     " + hour + ":" + minute
        default:
            return "nothing Alarm"
        }
    }
    
    static func newScheduleLocal(snooze:Snooze) {
        
        //Date component trigger
        var dateComponents = DateComponents()
        // a more realistic example for Gregorian calendar. Every Monday at 11:30AM
        dateComponents.hour = snooze.hour
        dateComponents.minute = snooze.minute
        dateComponents.weekday = snooze.weekday
        dateComponents.second = 0
        
        let content = UNMutableNotificationContent()
        
        content.title = "Snooze"
        content.subtitle = "⏰"
        content.body = snooze.body!
        content.sound = UNNotificationSound(named: snooze.sound!)
        // mathContent.badge = 1
        content.categoryIdentifier = "snooze"
        
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents,repeats: snooze.repeatAlarm!)
        
        //let requestIdentifier = uuid
        let request = UNNotificationRequest(identifier: snooze.uuid!, content: content, trigger: trigger)
        
        
        UNUserNotificationCenter.current().add(request) { (error) in
            
            print(error as Any)
        }
    }
    
    static func removeAll() {
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
    }
    static func removeNotif(uuid:String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [uuid])

    }
    
     func fetchNotif() -> Observable<[Snooze]> {
        return Observable<[Snooze]>.create({ (observer) -> Disposable in
        var listNotifications = [Snooze]()
           // var listNotifications = Variable<[String]>([String]())

        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler:  {
            (requests) in
            //var displayBody = ""
            //var displayUUID = ""
            for request in requests{
                // displayUUID += request.identifier + "\t"
                //displayUUID += request.identifier + "\n"
                //displayBody += request.content.body + "\n"
                let oneNotif = Snooze()
                oneNotif.body = request.content.body
                oneNotif.uuid = request.identifier
                listNotifications.append(oneNotif)
                //print(listNotifications)
                //print(displayBody)
            }
            observer.onNext(listNotifications)
            observer.onCompleted()
        })
            return Disposables.create {
                //request.cancel()
            }
        })
    }
    /*
    static func fetchNotif(completionHandler: @escaping ([String])->()) {
        var listNotifications = [String]()
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler:  {
            (requests) in
            //var displayBody = ""
            //var displayUUID = ""

            for request in requests{
               // displayUUID += request.identifier + "\t"
                //displayUUID += request.identifier + "\n"
                //displayBody += request.content.body + "\n"
                listNotifications.append(request.content.body)
                //print(listNotifications)

                //print(displayBody)
            }
            completionHandler(listNotifications)

        })

}
    */
    /*
    
    static func fetchNotif() -> [String] {
        var listNotifications = [String]()
        UNUserNotificationCenter.current().getPendingNotificationRequests {
            (requests) in
            //var displayBody = ""
            //var displayUUID = ""
            
            for request in requests{
                // displayUUID += request.identifier + "\t"
                //displayUUID += request.identifier + "\n"
                //displayBody += request.content.body + "\n"
                listNotifications.append(request.content.body)
                //print(listNotifications)
                
                //print(displayBody)
            }
            
        }
        print(listNotifications)
        
        return listNotifications
    }
    */
    
    
}
*/
