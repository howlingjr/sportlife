//
//  TabBarItem.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//

import UIKit

enum TabBarItem: CaseIterable {
    
    static let dimension: CGFloat = 35
    
    case day
    //TODO: autres cases  standing, playoffs ?
  
    public var title: String {
        switch self {
        case .day:
            return "matchs"
        }
    }
        
    public var image: UIImage {
        switch self {
        case .day:
            return imageItem("panneau")
        }
    }
    
    public var selectedImage: UIImage {
        switch self {
        case .day:
            return imageItem("panneau")
        }
    }
    
    private func imageItem(_ name: String) -> UIImage {
        UIImage(named: name)?.imageResized(to: CGSize(width: TabBarItem.dimension, height: TabBarItem.dimension)).withRenderingMode(.alwaysOriginal) ?? UIImage()
    }
}

extension UIImage {
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
