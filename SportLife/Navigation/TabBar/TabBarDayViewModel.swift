//
//  TabBarHomeViewModel.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//
import UIKit

class TabBarDayViewModel: TabBarDayViewModelProtocol {
    let items: [NavigationItemProtocol]
    let rootControllers: [UIViewController]
    var rootNavigationControllers: [UINavigationController] {
        rootControllers.compactMap { $0 as? UINavigationController }
    }
    
    init(items: [NavigationItemProtocol]) {
        self.items = items
        self.rootControllers = items.map { _ in UINavigationController() }
    }
}
