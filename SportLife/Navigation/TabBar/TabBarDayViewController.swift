//
//  TabBarHomeViewController.swift
//  DA100
//
//  Created by Adscientiam on 11/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import UIKit
import SwiftUI

protocol TabBarDayViewControllerDelegate: AnyObject {
    func onTabItemSelected(at index: Int)
}

class TabBarDayViewController: UITabBarController {

    // MARK: - Properties
    var viewModel: TabBarDayViewModelProtocol?
    weak var navigationDelegate: TabBarDayViewControllerDelegate?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBar.backgroundColor = .white
        tabBar.isTranslucent = false
    }
    
    // MARK: - public methods
    /**
     select index of HomeTabBarController
     */
    public func selectIndex(_ index: Int) {
        self.selectedIndex = index
    }

    /**
     Set controller's ViewControllers and TabBarItems and manage tab bar appearance
     */
    public func setupTabBar() {
        guard let viewModel = viewModel else { return }
       
        viewModel.rootControllers.enumerated().forEach {
            let item = viewModel.items[$0.offset]
            $0.element.tabBarItem = UITabBarItem(title: item.title, image: item.image, selectedImage: item.selectedImage)
        }
        setViewControllers(viewModel.rootControllers, animated: false)
        navigationDelegate?.onTabItemSelected(at: 0)
    }
}

// MARK: - UITabBarControllerDelegate
extension TabBarDayViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let index = tabBarController.viewControllers?.firstIndex(of: viewController) {
          // let viewModel = viewModel {
            updateTabBarStyle(.blue, isReversed: false)

//            let item = viewModel.items[index]
//            if ((item.module as? SettingsModuleProtocol) != nil) {
//                updateTabBarStyle(.mainBlue, isReversed: true)
//            } else {
//                updateTabBarStyle(.white, isReversed: false)
//            }
            navigationDelegate?.onTabItemSelected(at: index)
        }
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard viewController == tabBarController.selectedViewController else {
            return true
        }
        return false
    }
    
    private func updateTabBarStyle( _ color: UIColor, isReversed: Bool) {
        guard let viewModel = viewModel else { return }
        tabBar.backgroundColor = color
        tabBar.barTintColor = color
        _ = TabBarItem.allCases.map { _ in
            for (index, item) in viewModel.items.enumerated() {
                tabBar.items?[index].image = item.image
            }
        }
    }
}
