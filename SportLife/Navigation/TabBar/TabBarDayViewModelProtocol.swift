//
//  TabBarHomeViewModelProtocol.swift
//  DA100
//
//  Created by Adscientiam on 11/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//
import UIKit

protocol TabBarDayViewModelProtocol {
    var items: [NavigationItemProtocol] { get }
    var rootControllers: [UIViewController] { get }
    var rootNavigationControllers: [UINavigationController] { get }
}
