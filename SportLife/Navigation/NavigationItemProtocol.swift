//
//  NavigationItemProtocol.swift
//  DA100
//
//  Created by Adscientiam on 11/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import UIKit

public protocol NavigationItemProtocol {
    var title: String { get }
    var image: UIImage? { get }
    var selectedImage: UIImage? { get }
    var module: ModuleProtocol { get }
}

public struct NavigationItem: NavigationItemProtocol {
    public var title: String
    public var image: UIImage?
    public var selectedImage: UIImage?
    public var module: ModuleProtocol

    public init(title: String, image: UIImage?, selectedImage: UIImage?, module: ModuleProtocol) {
        self.title = title
        self.image = image
        self.selectedImage = selectedImage
        self.module = module
    }
}
