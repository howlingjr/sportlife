//
//  TabBarDIManagerProtocol.swift
//  DA100
//
//  Created by Adscientiam on 11/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import Swinject
import UIKit

enum IosCoreTabBarControllersType {
    case day
}

protocol TabBarDIManagerProtocol: DIManagerProtocol {
    func controller(for type: IosCoreTabBarControllersType) -> UIViewController?
    var modules: [ModuleProtocol] { get }
}

struct TabBarDIManager: TabBarDIManagerProtocol {

    internal var container: Container
    var modules: [ModuleProtocol] {
        container.resolve([NavigationItemProtocol].self)?.map { $0.module } ?? []
    }

    init(container: Container) {
        self.container = Container(parent: container)
        registerViewModels(self.container)
        registerViews(self.container)
    }

    internal func registerViewModels(_ container: Container) {
        container.register(TabBarDayViewModelProtocol.self) {
            TabBarDayViewModel(items: $0.resolve([NavigationItemProtocol].self) ?? [])
        }.inObjectScope(.container)
    }

    internal func registerViews(_ container: Container) {
        container.register(TabBarDayViewController.self) { resolver in
            let controller = TabBarDayViewController()
            controller.viewModel = resolver.resolve(TabBarDayViewModelProtocol.self)
            return controller
        }
    }

    func controller(for type: IosCoreTabBarControllersType) -> UIViewController? {
        switch type {
        case .day:
            return container.resolve(TabBarDayViewController.self)
        }
    }
}
