//
//  TabBarCoordinator.swift
//  DA100
//
//  Created by Adscientiam on 11/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import Swinject
import UIKit

extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

open class TabBarCoordinator: BaseCoordinator {

    // MARK: - private properties
    private let router: Router
    private var diManager: TabBarDIManagerProtocol

    public override var finishFlow: Closure? {
        didSet {
            guard let day = diManager.controller(for: .day) as? TabBarDayViewController else { return }
            // Define the router for each module
            day.viewModel?.rootNavigationControllers.enumerated()
                .forEach { (index, controller) in
                    let module = diManager.modules[safe: index]
                    module?.coordinator?.finishFlow = finishFlow
            }
        }
    }
    
    public init(router: Router, container: Container) {
        self.router = router
        self.diManager = TabBarDIManager(container: container)
        super.init(container: container)
        initModules()
    }

    // MARK: - public methods
    open override func start() {
        showHome()
    }

    func showHome() {
        if let day = diManager.controller(for: .day) as? TabBarDayViewController {
            day.navigationDelegate = self
            day.setupTabBar()
            router.setRootModule(day)
        }
    }

    // MARK: - private methods
    /**
     Initialize each modules by giving them a router and
     adding them to the dependendies
     */
    private func initModules() {
        guard let home = diManager.controller(for: .day) as? TabBarDayViewController else { return }
        // Define the router for each module
        home.viewModel?.rootNavigationControllers.enumerated()
            .forEach { (index, controller) in
                let module = diManager.modules[safe: index]
                module?.router = RouterImp(rootController: controller)
            }
    }

}

extension TabBarCoordinator: TabBarDayViewControllerDelegate {
    func onTabItemSelected(at index: Int) {
        guard let module = diManager.modules[safe: index] else {return}
        module.start()
    }
}
