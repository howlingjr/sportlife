//
//  MatchViewController.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
import RxCocoa
import RxAnimated
import Alamofire
//import RxDataSources

class MatchViewController: UIViewController {
    
    @IBOutlet weak var matchTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    
    var yesterday = ""
    var today = ""
    var id = ""
    var flagDay = ""
    var matchForDetail : Match!
    
    var choiceDay : Observable<String?>{
        return segmentedControl.rx.value.repeatEvery(period: 15, scheduler: MainScheduler.instance).map {
            (selectedIndex: Int) -> String in
            if selectedIndex == 0 {
                print("match yesterday")
                self.flagDay = self.yesterday
                return self.yesterday
            }else {
                print("match today")
                self.flagDay = self.today
                return self.today
                
                
            }
        }
        
    }
    
    /*
     var choiceDay : Observable<String?>{
     return dayButton.rx.tap.asObservable().repeatEvery(period: 3, scheduler: MainScheduler.instance)
     .map { (_) -> String in
     return self.today
     }
     }
     */
    
    /*
     var choiceDay : Observable<String?> {
     return Observable.just(self.today).repeatEvery(period: 3, scheduler: MainScheduler.instance)
     }
     */
    
    var matchViewModel : MatchViewModel!
    let disposeBag = DisposeBag()
    
    var timer: Observable<NSInteger>!
    
    func setupRx() {
        matchViewModel = MatchViewModel(withNameObservable: choiceDay)
        
        /*
         dayButton.rx.tap
         .subscribe({_ in
         self.timerReload()
         })
         .disposed(by: disposeBag)
         */
        
        matchViewModel
            .rx_matchs
            .drive(matchTableView.rx.items(cellIdentifier: "MatchTableViewCell", cellType: MatchTableViewCell.self))
            { (_, item, cell) in
                cell.match = item
            }
            /*
             .drive(matchTableView.rx.items) { (tableView, i, match) in
             let cell = tableView.dequeueReusableCell(withIdentifier: "MatchTableViewCell", for: IndexPath(row: i, section: 0)) as! MatchTableViewCell
             cell.homeNicknameLabel?.text = match.homeNickname
             cell.homeScoreLabel?.text = match.homeScore
             return cell
             }
             */
            .disposed(by: disposeBag)
        
        
        matchTableView.rx.modelSelected(Match.self).subscribe(onNext: {
            match in
            self.id = match.id!
            self.matchForDetail = match

        }).disposed(by: disposeBag)
        
        
        matchTableView.rx.itemSelected.subscribe(onNext: { [weak self]
            indexPath in
            self?.performSegue(withIdentifier: "showDetail", sender: indexPath)
            self?.matchTableView.deselectRow(at: indexPath, animated: true)
        }).disposed(by: disposeBag)
        
    }
    
    
    func timerReload() {
        timer = Observable<NSInteger>.interval(1.0, scheduler: MainScheduler.instance).repeatEvery(period: 15, scheduler: MainScheduler.instance)
            .filter {$0 < 15}
        
        timer.map { "\(13 - $0) s " }
            .bind(animated: countdownLabel.rx.animated.fade(duration: 0.33).text)
            //.bind(animated: countdownLabel.rx.animated.flip(.top,duration: 0.33).text)
            .disposed(by: disposeBag)
        
        
        /*
         timer.subscribe(onNext: { secs -> Void in
         print("\(13 - secs) s")
         // self.countdownLabel.text = "\(13 - secs) s"
         })
         .disposed(by: disposeBag)
         */
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let destination = segue.destination as? MatchDetailViewController {
                destination.id = id
                destination.flagDay = flagDay
                destination.matchForDetail = matchForDetail
              
            }
        } else {
            print("segue inexistant")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.displayNavbar(title: "MATCHS", imageBack: UIImage())
        segmentedControl.setTitle("main.yesterday".translate, forSegmentAt: 0)
        segmentedControl.setTitle("main.today".translate, forSegmentAt: 1)
        updateLabel.text = "main.update".translate

        (today,yesterday) = DateTools.fetchYesterdayToday()
        matchTableView.register(UINib(nibName: "MatchTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchTableViewCell")
        //matchTableView.allowsSelection = false
        
        setupRx()
        timerReload()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if NetworkReachabilityManager()!.isReachable == false {
            self.title = "main.notconnected".translate
        }else{
            self.title = "MATCHS"
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
*/
