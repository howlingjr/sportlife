//
//  MatchDetailViewController.swift
//  SportLife
//
//  Created by ludovic grimbert on 18/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
import RxCocoa

class MatchDetailViewController: UIViewController {
    
    @IBOutlet weak var nameLeaderHomePointsTextView: UITextView!
    @IBOutlet weak var valueLeaderHomePointsLabel: UILabel!
    @IBOutlet weak var nameLeaderHomeAssistsTextView: UITextView!
    @IBOutlet weak var valueLeaderHomeAssistsLabel: UILabel!
    @IBOutlet weak var nameLeaderHomeReboundsTextView: UITextView!
    @IBOutlet weak var valueLeaderHomeReboundsLabel: UILabel!
    
    @IBOutlet weak var nameLeaderVisitorPointsTextView: UITextView!
    @IBOutlet weak var valueLeaderVisitorPointsLabel: UILabel!
    @IBOutlet weak var nameLeaderVisitorAssistsTextView: UITextView!
    @IBOutlet weak var valueLeaderVisitorAssistsLabel: UILabel!
    @IBOutlet weak var nameLeaderVisitorReboundsTextView: UITextView!
    @IBOutlet weak var valueLeaderVisitorReboundsLabel: UILabel!
    
    @IBOutlet weak var visitorNameLabel: UILabel!
    @IBOutlet weak var homeNameLabel: UILabel!
    @IBOutlet weak var visitorImageView: UIImageView!
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet weak var visitorScoreLabel: UILabel!
    @IBOutlet weak var homeScoreLabel: UILabel!
    
    var id: String!
    var flagDay: String!
    var matchForDetail: Match!
    
    var viewModel : MatchDetailViewModel!
    let disposeBag = DisposeBag()
    
    
    fileprivate func initializeUIBindings() {
        
        viewModel = MatchDetailViewModel(matchDetailService: MatchDetailService(),date : flagDay,matchId:id)
        
        
        //Home
        viewModel.detailNameLeaderHomePoints
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderHomePointsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderHomePoints
            .subscribe(onNext: { value -> Void in
                self.valueLeaderHomePointsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.detailNameLeaderHomeAssists
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderHomeAssistsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderHomeAssists
            .subscribe(onNext: { value -> Void in
                self.valueLeaderHomeAssistsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.detailNameLeaderHomeRebounds
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderHomeReboundsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderHomeRebounds
            .subscribe(onNext: { value -> Void in
                self.valueLeaderHomeReboundsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        //Visitor
        viewModel.detailNameLeaderVisitorPoints
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderVisitorPointsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderVisitorPoints
            .subscribe(onNext: { value -> Void in
                self.valueLeaderVisitorPointsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.detailNameLeaderVisitorAssists
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderVisitorAssistsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderVisitorAssists
            .subscribe(onNext: { value -> Void in
                self.valueLeaderVisitorAssistsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.detailNameLeaderVisitorRebounds
            .subscribe({ value -> Void in
                var totalName = ""
                for element in value.element! {
                    print(element.firstName!)
                    print(element.lastName!)
                    totalName += (element.firstName! + " " + element.lastName! + "\n")
                }
                self.nameLeaderVisitorReboundsTextView.text = totalName
            })
            .disposed(by: disposeBag)
        
        
        viewModel.detailValueLeaderVisitorRebounds
            .subscribe(onNext: { value -> Void in
                self.valueLeaderVisitorReboundsLabel.text = value
            })
            .disposed(by: disposeBag)
        
        
 
        
       
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.displayNavbar(title: "PLAYERS", imageBack: #imageLiteral(resourceName: "back"))
        
        visitorNameLabel.text = matchForDetail.visitorNickname
        homeNameLabel.text = matchForDetail.homeNickname
        
        visitorScoreLabel.text = matchForDetail.visitorScore
        homeScoreLabel.text = matchForDetail.homeScore
        
        Logo.displayLogo(nickname: matchForDetail.visitorNickname, logoImageView: visitorImageView)
        Logo.displayLogo(nickname: matchForDetail.homeNickname, logoImageView: homeImageView)

        Winner.displayRed(homeScore: matchForDetail.homeScore!, visitorScore: matchForDetail.visitorScore!, homeNameLabel: homeNameLabel, visitorNameLabel: visitorNameLabel)
        
        initializeUIBindings()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
*/
