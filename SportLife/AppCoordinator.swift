//
//  AppCoordinator.swift
//  SportLife
//
//  Created by Ludovic Grimbert on 06/02/2022.
//  Copyright © 2022 ludovic grimbert. All rights reserved.
//

import Foundation
import Swinject


class AppCoordinator: BaseCoordinator {
    
    private let router: Router
    
    // MARK: - life cycle
    public init(router: Router, container: Container) {
        self.router = router
        super.init(container: container)
    }
    
    override func start() {
        self.runNavigationFlow()
    }
    
    func runNavigationFlow() {
        guard var navigation = container.resolve(NavigationProtocol.self),
              let coordinator = navigation.coordinator else { return }

        addDependency(coordinator)
        navigation.finishFlow = { [weak self] in
            guard let self = self else { return }
            self.removeDependency(coordinator)
            
            //TODO: quoi presenter quand logout ?
           // self.presentOnboardingFlow()
        }
        
        DispatchQueue.main.async {
            navigation.start()
        }
    }
    
}
