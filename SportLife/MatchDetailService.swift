//
//  MatchService.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import Alamofire
import ObjectMapper
import RxSwift
import SwiftyJSON




class MatchDetailService {
    
    //Home
    func fetchNameLeaderHomePoints(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                         let result = Mapper<NameLeaderHomePointsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                        observer.onNext(leader)
                        observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderHomePoints(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderHomePoints>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchNameLeaderHomeAssists(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<NameLeaderHomeAssistsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderHomeAssists(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderHomeAssists>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchNameLeaderHomeRebounds(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<NameLeaderHomeReboundsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderHomeRebounds(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderHomeRebounds>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    //Visitor
    func fetchNameLeaderVisitorPoints(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<NameLeaderVisitorPointsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderVisitorPoints(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderVisitorPoints>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchNameLeaderVisitorAssists(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<NameLeaderVisitorAssistsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderVisitorAssists(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderVisitorAssists>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    func fetchNameLeaderVisitorRebounds(date:String,matchId:String) -> Observable<[CV]> {
        return Observable<[CV]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<NameLeaderVisitorReboundsResponse>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.leader {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchValueLeaderVisitorRebounds(date:String,matchId:String) -> Observable<String> {
        return Observable<String>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/noseason/game/\(date)/\(matchId)/boxscore.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<ValueLeaderVisitorRebounds>().map(JSON: (json.dictionaryObject)!)!
                        if let leader = result.stat {
                            observer.onNext(leader)
                            observer.onCompleted()
                        }
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    
    
}

*/
