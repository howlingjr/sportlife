//
//  StandingsViewController.swift
//  SportLife
//
//  Created by ludovic grimbert on 02/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
import RxCocoa
import Crashlytics
//import RxDataSources

class StandingViewController: UIViewController {

    @IBOutlet weak var standingWestTableView: UITableView!
    @IBOutlet weak var standingEastTableView: UITableView!
    
    let viewModel = StandingViewModel(standingService: StandingService())
    let disposeBag = DisposeBag()
 
    fileprivate func initializeUIBindings() {
        /*
        viewModel.standingWestVariable.asObservable().subscribe(onNext: {(element) in
        }).disposed(by: disposeBag)
        */
        viewModel.standingWest
            //.map { $0 }
            .bind(to:standingWestTableView.rx.items(cellIdentifier: "StandingWestTableViewCell", cellType: StandingWestTableViewCell.self))
            { (_, item, cell) in
                cell.standing = item
            }
            //.throttle(0.3, scheduler: MainScheduler.instance)
           //.subscribe(onNext: { print($0.count) })
            .disposed(by: disposeBag)
        
        viewModel.standingEast
            .bind(to:standingEastTableView.rx.items(cellIdentifier: "StandingEastTableViewCell", cellType: StandingEastTableViewCell.self))
            { (_, item, cell) in
                cell.standing = item
            }
            .disposed(by: disposeBag)
        
    }
    
    /*
    @IBAction func testCrash(_ sender: Any) {
            Crashlytics.sharedInstance().crash()
    }
 */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayNavbar(title: "STANDINGS", imageBack: UIImage())

        initializeUIBindings()

        standingWestTableView.register(UINib(nibName: "StandingWestTableViewCell", bundle: nil), forCellReuseIdentifier: "StandingWestTableViewCell")
        standingWestTableView.allowsSelection = false
        standingEastTableView.register(UINib(nibName: "StandingEastTableViewCell", bundle: nil), forCellReuseIdentifier: "StandingEastTableViewCell")
        standingEastTableView.allowsSelection = false


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
*/
