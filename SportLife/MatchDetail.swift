//
//  MatchDetail.swift
//  SportLife
//
//  Created by ludovic grimbert on 18/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import ObjectMapper

class CV : Mappable {
    var firstName : String?
    var lastName : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        firstName <- map["FirstName"]
        lastName <- map["LastName"]
    }
}

//home
class NameLeaderHomePointsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.home.Leaders.Points.leader"]
    }
}

class ValueLeaderHomePoints : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.home.Leaders.Points.StatValue"]
    }
}


class NameLeaderHomeAssistsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.home.Leaders.Assists.leader"]
    }
}

class ValueLeaderHomeAssists : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.home.Leaders.Assists.StatValue"]
    }
}
class NameLeaderHomeReboundsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.home.Leaders.Rebounds.leader"]
    }
}

class ValueLeaderHomeRebounds : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.home.Leaders.Rebounds.StatValue"]
    }
}


//Visitor

class NameLeaderVisitorPointsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.visitor.Leaders.Points.leader"]
    }
}

class ValueLeaderVisitorPoints : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.visitor.Leaders.Points.StatValue"]
    }
}


class NameLeaderVisitorAssistsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.visitor.Leaders.Assists.leader"]
    }
}

class ValueLeaderVisitorAssists : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.visitor.Leaders.Assists.StatValue"]
    }
}
class NameLeaderVisitorReboundsResponse: Mappable {
    var leader : Array<CV>?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        leader <- map["sports_content.game.visitor.Leaders.Rebounds.leader"]
    }
}

class ValueLeaderVisitorRebounds : Mappable {
    var stat : String?
    init() { }
    required init?(map: Map) { }
    func mapping(map: Map) {
        stat <- map["sports_content.game.visitor.Leaders.Rebounds.StatValue"]
    }
}

*/


