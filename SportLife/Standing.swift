//
//  Standings.swift
//  SportLife
//
//  Created by ludovic grimbert on 02/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import ObjectMapper

class StandingWestResponse: Mappable {
    
    var team : Array<Standing>?
   
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        team <- map["sports_content.standings.conferences.West.team"]
        
    }
}


class StandingEastResponse: Mappable {
    
    var team : Array<Standing>?
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        team <- map["sports_content.standings.conferences.East.team"]
        
    }
}

class Standing: Mappable {
    
    var nickname : String?
    var wins : String?
    var losses : String?
    var rank : String?

    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        nickname <- map["nickname"]
        wins <- map["team_stats.wins"]
        losses <- map["team_stats.losses"]
        rank <- map["team_stats.rank"]

    }
}
*/
