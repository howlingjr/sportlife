//
//  StandingsService.swift
//  SportLife
//
//  Created by ludovic grimbert on 02/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import Alamofire
import ObjectMapper
import RxSwift
import SwiftyJSON

class StandingService {
    
    func fetchStandingWest(_ date:String) -> Observable<[Standing]> {
        return Observable<[Standing]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/\(date)/standings/conference.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<StandingWestResponse>().map(JSON: (json.dictionaryObject)!)!
                        observer.onNext(result.team!)
                        observer.onCompleted()
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
    func fetchStandingEast(_ date:String) -> Observable<[Standing]> {
        return Observable<[Standing]>.create({ (observer) -> Disposable in
            let request = Alamofire
                .request("http://data.nba.com/json/cms/\(date)/standings/conference.json",method: .get)
                .responseJSON(completionHandler: { (response) in
                    if let values = response.result.value  {
                        let json = JSON(values)
                        let result = Mapper<StandingEastResponse>().map(JSON: (json.dictionaryObject)!)!
                        observer.onNext(result.team!)
                        observer.onCompleted()
                    } else {
                        observer.onError(response.result.error!)
                    }
                })
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
    
}
*/
