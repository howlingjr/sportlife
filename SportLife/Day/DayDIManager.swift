//
//  HomeDIManager.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//

import Swinject
import UIKit

struct DayDIManager: DayDIManagerProtocol {
   
    internal var container: Container

    init(container: Container) {
        self.container = Container(parent: container)
       // registerViewModels(self.container)
        registerViews(self.container)
    }
    
    internal func registerViews(_ container: Container) {
        container.register(DayViewController.self) { resolver in
            DayViewController()
        }
    }
    
    func controller(for type: DayControllersType) -> UIViewController? {
        switch type {
        case .day:
            return container.resolve(DayViewController.self)
        }
    }
}
