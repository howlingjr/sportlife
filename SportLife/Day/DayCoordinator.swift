//
//  HomeCoordinator.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//

import Swinject
import UIKit

protocol DayCoordinatorDelegate: AnyObject {
}

class DayCoordinator: BaseCoordinator {
    private let router: Router
    private var diManager: DayDIManagerProtocol

    // MARK: - Life cycle
    public init(router: Router, container: Container) {
        self.router = router
        self.diManager = DayDIManager(container: container)
        super.init(container: container)
    }
    
    // MARK: - public methods
    open override func start() {
        if let day = diManager.controller(for: .day) as? DayViewController {
            router.setRootModule(day)
        }
    }
}
