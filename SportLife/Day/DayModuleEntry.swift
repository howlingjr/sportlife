//
//  HomeModuleEntry.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//

import Swinject

public class DayModuleEntry: DayModuleProtocol {
    public var coordinator: BaseCoordinatorProtocol?

    public var router: Router? {
        didSet {
          // Setting router will create a new instance of its coordinator
          initCoordinator()
        }
    }

    private var container: Container

    public var finishFlow: Closure? {
        didSet {
            (self.coordinator as? DayCoordinator)?.finishFlow = finishFlow
        }
    }

    public init(router: Router, container: Container) {
        self.container = container
        ({self.router = router})() // workaround to have didset in init
    }

    public init(container: Container) {
        self.container = container
    }

    private func initCoordinator() {
        guard let router = router else { return }
        coordinator = DayCoordinator(router: router, container: container)
        coordinator?.finishFlow = finishFlow
    }

    public func start() {
        coordinator?.start()
    }
}
