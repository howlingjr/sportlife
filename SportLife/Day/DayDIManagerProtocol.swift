//
//  HomeDIManagerProtocol.swift
//  DA100
//
//  Created by Adscientiam on 19/01/2022.
//

import UIKit

enum DayControllersType {
    case day
}

protocol DayDIManagerProtocol: DIManagerProtocol {
    func controller(for type: DayControllersType) -> UIViewController?
}
