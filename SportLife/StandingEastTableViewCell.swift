//
//  StandingEastTableViewCell.swift
//  SportLife
//
//  Created by ludovic grimbert on 03/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit

class StandingEastTableViewCell: UITableViewCell {
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var winsLossesLabel: UILabel!
    @IBOutlet weak var logoImageView:UIImageView!
    
    var standing: Standing? {
        didSet {
            
            nicknameLabel.text = standing?.nickname
            rankLabel.text = (standing?.rank)!
            winsLossesLabel.text = (standing?.wins)! + "-" + (standing?.losses)!
            Logo.displayLogo(nickname: (standing?.nickname)!, logoImageView: logoImageView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nicknameLabel.adjustsFontSizeToFitWidth = true
        nicknameLabel.text = ""
        rankLabel.adjustsFontSizeToFitWidth = true
        rankLabel.text = ""
        winsLossesLabel.adjustsFontSizeToFitWidth = true
        winsLossesLabel.text = ""
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
*/
