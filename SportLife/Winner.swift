//
//  Winner.swift
//  SportLife
//
//  Created by ludovic grimbert on 20/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation
import  UIKit

class Winner {
    
    static func displayRed(homeScore:String,visitorScore:String,homeNameLabel:UILabel,visitorNameLabel:UILabel) {

        
        if  (homeScore != "") && (visitorScore != "") {
            
            if Int(homeScore)! > Int(visitorScore)! {
                homeNameLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                visitorNameLabel.textColor = #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1)
            }else {
                visitorNameLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                homeNameLabel.textColor = #colorLiteral(red: 0.09295182675, green: 0.2951838374, blue: 0.6570760012, alpha: 1)
            }
        }else{
            homeNameLabel.textColor = #colorLiteral(red: 0.09295182675, green: 0.2951838374, blue: 0.6570760012, alpha: 1)
            visitorNameLabel.textColor = #colorLiteral(red: 0.09295182675, green: 0.2951838374, blue: 0.6570760012, alpha: 1)
        }
        
}

}
