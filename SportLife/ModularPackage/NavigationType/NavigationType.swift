//
//  NavigationType.swift
//  DA100
//
//  Created by Adscientiam on 10/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

public enum NavigationType {
    case root, push, present
}
