//
//  DIManagerProtocol.swift
//  DA100
//
//  Created by Adscientiam on 10/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import Swinject

public protocol DIManagerProtocol {
    var container: Container { get set }
    func registerViews(_ container: Container)
    func registerViewModels(_ container: Container)
    func cleanContainer()
}

// To make function optionnal
public extension DIManagerProtocol {
    func registerViews(_ container: Container) {}
    func registerViewModels(_ container: Container) {}
    func cleanContainer() {
        container.removeAll()
    }
}
