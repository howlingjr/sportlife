//
//  Router.swift
//
//  Created by Sebastien Gamel on 09/01/2022.
//

import UIKit

public protocol Presentable {
    func toPresent() -> UIViewController?
}

public protocol Router: AnyObject, Presentable {
    func present(_ module: Presentable?)
    func push(_ module: Presentable?)
    func push(_ module: Presentable?, animated: Bool)
    func push(_ module: Presentable?, hideBottomBar: Bool)
    func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?)

    func popModule()
    func popModule(animated: Bool)
    func popToLastModule<T: Presentable>(ofType type: T.Type, animated: Bool)
    func popToRootModule(animated: Bool)

    func setRootModule(_ module: Presentable?)
    func dismissModule()
    func current() -> Presentable?

}

public class RouterImp: NSObject, Router {
    
    // MARK: - properties
    private weak var rootController: UINavigationController?
    private var completions: [UIViewController : () -> Void]

    var delegate: UINavigationControllerDelegate? {
        get { rootController?.delegate }
        set { rootController?.delegate = newValue }
    }
    
    // MARK: - life cycle
    public init(rootController: UINavigationController, navigationBar isHidden: Bool = true) {
        self.rootController = rootController
        self.rootController?.navigationBar.isHidden = isHidden
        completions = [:]
    }
    
    // MARK: - Functions
    public func toPresent() -> UIViewController? {
        rootController
    }
    
    public func current() -> Presentable? {
        rootController?.visibleViewController
    }
    
    public func present(_ module: Presentable?) {
        guard let controller = module?.toPresent() else { return }
        rootController?.present(controller, animated: true)
    }
    
    
    public func push(_ module: Presentable?) {
        push(module, animated: true)
    }

    public func push(_ module: Presentable?, hideBottomBar: Bool) {
        push(module, animated: true, hideBottomBar: hideBottomBar, completion: nil)
    }

    public func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, hideBottomBar: false, completion: nil)
    }

    public func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?) {
        guard
            let controller = module?.toPresent(),
            (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }

        if let completion = completion {
            completions[controller] = completion
        }
        controller.hidesBottomBarWhenPushed = hideBottomBar
        rootController?.pushViewController(controller, animated: animated)
    }

    
    public func popModule() {
        popModule(animated: true)
    }

    public func popModule(animated: Bool) {
        // Manage fromAnotherModule navigation type -> pop automatically to previous module
        if rootController?.viewControllers.count == 1 {
            rootController?.viewControllers.first?.popToPreviousModule()
        }

        if let controller = rootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    public func popToLastModule<T: Presentable>(ofType type: T.Type, animated: Bool) {
        guard let module = rootController?.viewControllers.last(where: { $0 is T }) else {
            return
        }

        rootController?.popToViewController(module, animated: true)
    }
    
    public func popToRootModule(animated: Bool) {
        if let controllers = rootController?.popToRootViewController(animated: animated) {
            controllers.forEach { controller in
                runCompletion(for: controller)
            }
        }
    }
    
    public func dismissModule() {
        rootController?.dismiss(animated: true)
    }
    
    public func setRootModule(_ module: Presentable?) {
        guard let controller = module?.toPresent() else { return }
        rootController?.setViewControllers([controller], animated: false)
    }
    
    // MARK:- private methods
    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }

}
