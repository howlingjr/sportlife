//
//  BaseCoordinatorProtocol.swift
//  DA100
//
//  Created by Adscientiam on 10/01/2022.
//  Copyright © 2022 Adscientiam. All rights reserved.
//

import UIKit

public protocol BaseCoordinatorProtocol: Coordinator, FlowOutput {

    /// this function can add  dependency to a specific coordinator
    func addDependency(_ coordinator: Coordinator)

    /// this function can remove dependency recursively to a specific coordinator
    func removeDependency(_ coordinator: Coordinator?)

    /// this function return the viewcontroller type for destination
    func viewcontrollerType<T>(for destination: T) -> UIViewController.Type?
}

public extension BaseCoordinatorProtocol {
    // make it optionnal
    func viewcontrollerType<T>(for destination: T) -> UIViewController.Type? {
        nil
    }
}
