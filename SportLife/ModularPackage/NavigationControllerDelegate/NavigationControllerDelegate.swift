//
//  NavigationControllerDelegate.swift
//  
//
//  Created by Adscientiam on 14/01/2022.
//

import UIKit

public protocol ADSNavigationDelegate: AnyObject {
    func navigateToPreviousModule()
}

public class NavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
    public weak var delegate: ADSNavigationDelegate?

    public init(delegate: ADSNavigationDelegate? = nil) {
        self.delegate = delegate
    }

    public func navigateToPreviousModule() {
        delegate?.navigateToPreviousModule()
    }
}
