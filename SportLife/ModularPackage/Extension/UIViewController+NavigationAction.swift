//
//  UIViewController+NavigationAction.swift
//  
//
//  Created by Adscientiam on 14/01/2022.
//

import UIKit

public extension UIViewController {
    @objc func popToPreviousModule() {
        (navigationController?.delegate as? NavigationControllerDelegate)?.navigateToPreviousModule()
    }

    @objc func popToPrevious() {
        navigationController?.popViewController(animated: true)
    }

}
