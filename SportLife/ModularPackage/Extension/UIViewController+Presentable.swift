//
//  UIViewController+Presentable.swift
//
//  Created by Sebastien Gamel on 09/01/2022.
//

import UIKit

extension UIViewController: Presentable {
    public func toPresent() -> UIViewController? {
        return self
    }
}
