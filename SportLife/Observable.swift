//
//  Observable.swift
//  SportLife
//
//  Created by ludovic grimbert on 16/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import RxSwift

extension ObservableType {
    /**
     Returns an observable sequence that repeats at each period.
     
     - seealso: [timer operator on reactivex.io](http://reactivex.io/documentation/operators/timer.html)
     
     - parameter period: Period to produce subsequent values.
     - returns: An observable sequence that repeat at each period.
     */
    
    public func repeatEvery(period: RxTimeInterval, scheduler: SchedulerType)
        -> Observable<E> {
          return Observable<Int>.timer(0, period: period, scheduler: scheduler)
               // return Observable<Int>.interval(5.0, scheduler: MainScheduler.instance)
                .flatMapLatest { _ in
                    return self
            }
    }
}

class Refreshable<T>: ObservableType {
    
    typealias E = T
    func subscribe<O:ObserverType>(_ observer: O) -> Disposable where O.E == E {
        return refreshMeans
            .flatMapLatest{ [unowned self] in
                return self.origin.asObservable()
            }
            .subscribe(observer)
    }
    
    private let disposeBag = DisposeBag()
    private let origin: Observable<T>
    init(origin: Observable<T>, updatedOn: Observable<Void> = .never()) {
        self.origin = origin
        updatedOn.bind(to: refreshMeans).disposed(by: disposeBag)
    }
    
    private let refreshMeans = BehaviorSubject<Void>(value: ())
    
    func refresh() {
        refreshMeans.onNext(())
    }
    
}
 */
