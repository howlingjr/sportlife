//
//  Match.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import ObjectMapper


class MatchResponse: Mappable {
    
    var game : Array<Match>?
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        game <- map["sports_content.games.game"]
        
    }
}

class Match: Mappable {
    
    var homeNickname : String?
    var homeScore : String?
    var id : String?
    var period : String?
    var visitorNickname : String?
    var visitorScore : String?
    
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        homeNickname <- map["home.nickname"]
        homeScore <- map["home.score"]
        id <- map["id"]
        period <- map["period_time.period_status"]
        visitorNickname <- map["visitor.nickname"]
        visitorScore <- map["visitor.score"]
        
        
    }
}
*/
