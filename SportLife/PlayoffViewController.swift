//
//  PlayoffViewController.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
import RxCocoa
//import RxDataSources

class PlayoffViewController: UIViewController {

    @IBOutlet weak var playoffTableView: UITableView!

    
    let viewModel = PlayoffViewModel(playoffService: PlayoffService())
    let disposeBag = DisposeBag()
    
    
    fileprivate func initializeUIBindings() {
       
        viewModel.playoff
            .bind(to:playoffTableView.rx.items(cellIdentifier: "PlayoffTableViewCell", cellType: PlayoffTableViewCell.self))
            { (_, item, cell) in
                cell.playoff = item
            }
            .disposed(by: disposeBag)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayNavbar(title: "PLAYOFFS", imageBack: UIImage())

        initializeUIBindings()
        
        playoffTableView.register(UINib(nibName: "PlayoffTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayoffTableViewCell")
        playoffTableView.allowsSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
*/
