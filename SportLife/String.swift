//
//  String.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation

//pour la traduction
extension String {
    var translate: String {
        let translated = NSLocalizedString(self, comment: "")
        
        if translated != self {
            return NSLocalizedString(self, comment: "")
        } else {
            print("*** Traduction non trouvée pour \(self)")
            return self
        }
    }
    
    func translate(comment: String) -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
