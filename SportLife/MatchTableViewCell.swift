//
//  MatchTableViewCell.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
import RxCocoa

class MatchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var homeNicknameLabel: UILabel!
    @IBOutlet weak var homeScoreLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var periodFrLabel: UILabel!
    @IBOutlet weak var visitorNicknameLabel: UILabel!
    @IBOutlet weak var visitorScoreLabel: UILabel!

    @IBOutlet weak var logo1ImageView:UIImageView!
    @IBOutlet weak var logo2ImageView:UIImageView!
    
    
    
    var match: Match? {
        didSet {
            homeNicknameLabel.text = match?.homeNickname
            homeScoreLabel.text = match?.homeScore
            visitorNicknameLabel.text = match?.visitorNickname
            visitorScoreLabel.text = match?.visitorScore
            periodLabel.text = match?.period
            periodFrLabel.text = renamePeriodeUSAtoFrance(periode:(match?.period)!)
            Logo.displayLogo(nickname: (match?.homeNickname)!, logoImageView: logo1ImageView)
            Logo.displayLogo(nickname: (match?.visitorNickname)!, logoImageView: logo2ImageView)

            Winner.displayRed(homeScore: (match?.homeScore)!, visitorScore: (match?.visitorScore)!, homeNameLabel: homeNicknameLabel, visitorNameLabel: visitorNicknameLabel)
        
        }
    }
    
    /*
    let disposeBag = DisposeBag()

    var match : Driver<Match>?{
        didSet {
            self.updateUI()
        }
    }
    private func updateUI(){

        match?
            .drive(onNext: {
                match in
                let homeNickname : Observable<String?> = Observable.of(match.homeNickname)
                let homeScore : Driver<String?> = Driver.of(match.homeScore)
                _ = homeNickname.bind(to: self.homeNicknameLabel.rx.text)
               // _ = homeScore.bind(to: self.homeScoreLabel.rx.text)
                _ = homeScore.drive(self.homeScoreLabel.rx.text)

            })
            .disposed(by: disposeBag)

    }
 */

   
    func renamePeriodeUSAtoFrance(periode:String)->String {
        var periodeFr = ""
        let changementHeure = periode
        if changementHeure.contains("ET") {
           // let changementHeureWithoutET = changementHeure.replacingOccurrences(of: " pm ET", with: "")
            let changementHeureWithoutET = changementHeure.replacingOccurrences(of: " ET", with: "").replacingOccurrences(of: " pm", with: "").replacingOccurrences(of: " PM", with: "")
            if let matchedIndex = changementHeureWithoutET.index(of: ":") {
                let post = String(changementHeureWithoutET[matchedIndex...])
                var heure = String(changementHeureWithoutET[..<matchedIndex])

                var minute = post.replacingOccurrences(of: ":", with: "")
                guard var heureToInt = Int(heure) else {return "0"}
                guard let minuteToInt = Int(minute) else {return "0"}
               
                
                switch heureToInt {
                case 6..<12:
                    heureToInt = heureToInt - 6
                    heure = "0" + "\(heureToInt)"
                case 0..<6:
                    heureToInt = heureToInt + 18
                    heure = "\(heureToInt)"
                case 12:
                    heureToInt = 18
                    heure = "\(heureToInt)"
                default:
                    break
                }
                
                if minuteToInt < 10 {
                    minute = "0" + "\(minuteToInt)"
                }
                periodeFr = "main.frenchhour".translate + heure + ":" + minute + " )"
            }
        } else {
            if changementHeure.contains("Final") {
                periodeFr = ""
            }else {
                periodeFr = ""
            }
        }
        return periodeFr
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        homeNicknameLabel.adjustsFontSizeToFitWidth = true
        homeNicknameLabel.text = ""
        homeScoreLabel.adjustsFontSizeToFitWidth = true
        homeScoreLabel.text = ""
        periodLabel.adjustsFontSizeToFitWidth = true
        periodLabel.text = ""
        visitorNicknameLabel.adjustsFontSizeToFitWidth = true
        visitorNicknameLabel.text = ""
        visitorScoreLabel.adjustsFontSizeToFitWidth = true
        visitorScoreLabel.text = ""
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
*/
