//
//  Date.swift
//  SportLife
//
//  Created by ludovic grimbert on 04/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation

class DateTools {
    
    static func fetchYear() {
        var year = ""
        let date = Date()
        let dateYesterday = date.addingTimeInterval(-24 * 60 * 60)
        let myDateYesterdayTotal = dateYesterday.absoluteDateToString
        ///let test = myDateYesterdayTotal.substring(from: 4)
        let indexStartOfText = myDateYesterdayTotal.index(myDateYesterdayTotal.startIndex, offsetBy: 4)
        let test = myDateYesterdayTotal[indexStartOfText...]
        guard let testToInt = Int(test) else {return}
        if testToInt > 1 && testToInt < 800 {
            var myDateInt = Int(dateYesterday.absoluteDateToString2)
            myDateInt = myDateInt! - 1
            year = "\(myDateInt!)"
            StandingSettings.sharedInstance.registerModeApp(newAppModeSelected: year)
        }else {
            year = dateYesterday.absoluteDateToString2
            StandingSettings.sharedInstance.registerModeApp(newAppModeSelected: year)
        }
        
    }
    
    static func fetchYesterdayToday()->(String,String){
        var dateString = ""
        var dateYesterdayString = ""
        let date = Date()
        let dateYesterday = date.addingTimeInterval(-24 * 60 * 60)
        dateYesterdayString = dateYesterday.absoluteDateToString
        dateString = date.absoluteDateToString
        
        return (dateString,dateYesterdayString)
    }
    
}

extension Date {
    /**
     * Renvoi une String avec la date en UTC
     */
    
    var absoluteDateToString : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: self)
    }
    
    var absoluteDateToString2 : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: self)
    }
    
    var absoluteDateToString3 : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: self)
    }
    
    var toSimpleDate : String {
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        return formatter.string(from: self)
    }
}
*/
