//
//  UIViewcontroller.swift
//  SportLife
//
//  Created by ludovic grimbert on 11/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
// MARK: - mes Fonctions
func displayNavbar(title:String,imageBack:UIImage) {
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.setNavigationBarHidden(false, animated:false)
    self.title = title
    
    let myBackButton:UIButton = UIButton.init(type: .custom)
    myBackButton.addTarget(self, action: #selector(popToVC(sender:)), for: .touchUpInside)
    // myBackButton.setTitle("<", for: .normal)
    myBackButton.setImage(imageBack, for: .normal)
    myBackButton.sizeToFit()
    
    //Add back button to navigationBar as left Button
    let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
    self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
}

@objc func popToVC(sender:UIBarButtonItem){
    //_ = self.navigationController?.popToRootViewController(animated: true)
    _ = self.navigationController?.popViewController(animated: true)
    
}
}
