//
//  SegmentedControl.swift
//  SportLife
//
//  Created by ludovic grimbert on 17/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation
import UIKit

class CustomSegmentedControl: UISegmentedControl {
    
    let selectedBackgroundColor = #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1)
    var sortedViews: [UIView]!
    var currentIndex: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        sortedViews = self.subviews.sorted(by:{$0.frame.origin.x < $1.frame.origin.x})
       // changeSelectedIndex(to: currentIndex)
        self.tintColor = #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1)
        self.layer.borderColor = #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1)
        self.layer.cornerRadius = 0
        self.clipsToBounds = true
        let unselectedAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                    NSAttributedStringKey.font:  UIFont(name: "HarmoniaSansProCyr-Black", size: 16)!]
        let unselectedAttributes2 = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.07555365242, green: 0.2486735356, blue: 0.5599947829, alpha: 1),
                                     NSAttributedStringKey.font:  UIFont(name: "HarmoniaSansProCyr-Black", size: 16)!] as [NSAttributedStringKey : Any]
        self.setTitleTextAttributes(unselectedAttributes2, for: .normal)
        self.setTitleTextAttributes(unselectedAttributes, for: .selected)
    }
    
    func changeSelectedIndex(to newIndex: Int) {
        sortedViews[currentIndex].backgroundColor = UIColor.clear
        currentIndex = newIndex
        self.selectedSegmentIndex = UISegmentedControlNoSegment
        sortedViews[currentIndex].backgroundColor = selectedBackgroundColor
    }
}
*/
