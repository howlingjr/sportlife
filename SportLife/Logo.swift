//
//  Logo.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation
import  UIKit

class Logo {
    
    static func displayLogo(nickname:String?,logoImageView:UIImageView) {
        
        switch nickname {
        case "Cavaliers"?:
            logoImageView.image = #imageLiteral(resourceName: "cavaliers")
        case "Bulls"?:
            logoImageView.image = #imageLiteral(resourceName: "bulls")
        case "Hawks"?:
            logoImageView.image = #imageLiteral(resourceName: "hawks")
        case "Lakers"?:
            logoImageView.image = #imageLiteral(resourceName: "lakers")
        case "Nuggets"?:
            logoImageView.image = #imageLiteral(resourceName: "nuggets")
        case "Wizards"?:
            logoImageView.image = #imageLiteral(resourceName: "wizards")
        case "Mavericks"?:
            logoImageView.image = #imageLiteral(resourceName: "mavericks")
        case "Pelicans"?:
            logoImageView.image = #imageLiteral(resourceName: "pelicans")
        case "Timberwolves"?:
            logoImageView.image = #imageLiteral(resourceName: "timberwolves")
        case "Trail Blazers"?:
            logoImageView.image = #imageLiteral(resourceName: "blazers")
        case "Warriors"?:
            logoImageView.image = #imageLiteral(resourceName: "warriors")
        case "Hornets"?:
            logoImageView.image = #imageLiteral(resourceName: "hornets")
        case "76ers"?:
            logoImageView.image = #imageLiteral(resourceName: "76ers")
        case "Heat"?:
            logoImageView.image = #imageLiteral(resourceName: "heat")
        case "Celtics"?:
            logoImageView.image = #imageLiteral(resourceName: "celtics")
        case "Bucks"?:
            logoImageView.image = #imageLiteral(resourceName: "bucks")
        case "Knicks"?:
            logoImageView.image = #imageLiteral(resourceName: "knicks")
        case "Rockets"?:
            logoImageView.image = #imageLiteral(resourceName: "rockets")
        case "Suns"?:
            logoImageView.image = #imageLiteral(resourceName: "suns")
        case "Clippers"?:
            logoImageView.image = #imageLiteral(resourceName: "clippers")
        case "Spurs"?:
            logoImageView.image = #imageLiteral(resourceName: "spurs")
        case "Grizzlies"?:
            logoImageView.image = #imageLiteral(resourceName: "grizzlies")
        case "Raptors"?:
            logoImageView.image = #imageLiteral(resourceName: "raptors")
        case "Kings"?:
            logoImageView.image = #imageLiteral(resourceName: "kings")
        case "Pacers"?:
            logoImageView.image = #imageLiteral(resourceName: "pacers")
        case "Nets"?:
            logoImageView.image = #imageLiteral(resourceName: "nets")
        case "Magic"?:
            logoImageView.image = #imageLiteral(resourceName: "magic")
        case "Thunder"?:
            logoImageView.image = #imageLiteral(resourceName: "thunder")
        case "Jazz"?:
            logoImageView.image = #imageLiteral(resourceName: "jazz")
        case "Pistons"?:
            logoImageView.image = #imageLiteral(resourceName: "pistons")
        default:
            break
        }
    }
    
    static func changeIdToTeam(id:String)-> String {
        var output = ""
        
        switch id {
        case "1610612737":
            output = "Hawks"
        case "1610612738":
            output = "Celtics"
        case "1610612751":
            output = "Nets"
        case "1610612766":
            output = "Hornets"
        case "1610612741":
            output = "Bulls"
        case "1610612739":
            output = "Cavaliers"
        case "1610612742":
            output = "Mavericks"
        case "1610612743":
            output = "Nuggets"
        case "1610612765":
            output = "Pistons"
        case "1610612744":
            output = "Warriors"
        case "1610612745":
            output = "Rockets"
        case "1610612754":
            output = "Pacers"
        case "1610612746":
            output = "Clippers"
        case "1610612747":
            output = "Lakers"
        case "1610612763":
            output = "Grizzlies"
        case "1610612748":
            output = "Heat"
        case "1610612749":
            output = "Bucks"
        case "1610612750":
            output = "Timberwolves"
        case "1610612740":
            output = "Pelicans"
        case "1610612752":
            output = "Knicks"
        case "1610612760":
            output = "Thunder"
        case "1610612753":
            output = "Magic"
        case "1610612755":
            output = "76ers"
        case "1610612756":
            output = "Suns"
        case "1610612757":
            output = "Trail Blazers"
        case "1610612758":
            output = "Kings"
        case "1610612759":
            output = "Spurs"
        case "1610612761":
            output = "Raptors"
        case "1610612762":
            output = "Jazz"
        case "1610612764":
            output = "Wizards"
        default:
            break
        }
        
        return output
    }
    
    
}
