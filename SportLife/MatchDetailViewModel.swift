//
//  MatchDetailViewModel.swift
//  SportLife
//
//  Created by ludovic grimbert on 18/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation
/*
import RxSwift
import Alamofire

class MatchDetailViewModel {
    
    fileprivate let matchDetailService: MatchDetailService
    
    fileprivate let disposeBag = DisposeBag()
    
    let matchNameVariable = Variable<[CV]>([CV]())
    let matchValueVariable = Variable<String>("")

    //Home
    let detailNameLeaderHomePoints: Observable<[CV]>
    let detailValueLeaderHomePoints: Observable<String>
    
    let detailNameLeaderHomeAssists: Observable<[CV]>
    let detailValueLeaderHomeAssists: Observable<String>
    
    let detailNameLeaderHomeRebounds: Observable<[CV]>
    let detailValueLeaderHomeRebounds: Observable<String>
    
    //Visitor
    let detailNameLeaderVisitorPoints: Observable<[CV]>
    let detailValueLeaderVisitorPoints: Observable<String>
    
    let detailNameLeaderVisitorAssists: Observable<[CV]>
    let detailValueLeaderVisitorAssists: Observable<String>
    
    let detailNameLeaderVisitorRebounds: Observable<[CV]>
    let detailValueLeaderVisitorRebounds: Observable<String>

    
    
    
    init(matchDetailService: MatchDetailService,date:String,matchId:String) {
        self.matchDetailService = matchDetailService
        
        //Home
        detailNameLeaderHomePoints = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderHomePoints(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderHomePoints = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderHomePoints(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        detailNameLeaderHomeAssists = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderHomeAssists(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderHomeAssists = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderHomeAssists(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        detailNameLeaderHomeRebounds = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderHomeRebounds(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderHomeRebounds = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderHomeRebounds(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        //Visitor
        detailNameLeaderVisitorPoints = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderVisitorPoints(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderVisitorPoints = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderVisitorPoints(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        detailNameLeaderVisitorAssists = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderVisitorAssists(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderVisitorAssists = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderVisitorAssists(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        detailNameLeaderVisitorRebounds = matchNameVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<[CV]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchNameLeaderVisitorRebounds(date: date, matchId: matchId)
                }
                return Observable.just([CV]())
        }
        
        detailValueLeaderVisitorRebounds = matchValueVariable.asObservable()
            //.repeatEvery(period: 30, scheduler: MainScheduler.instance)
            .flatMapLatest { _ -> Observable<String> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return matchDetailService.fetchValueLeaderVisitorRebounds(date: date, matchId: matchId)
                }
                return Observable.just("")
        }
        
        
        
        
    }
}

*/

