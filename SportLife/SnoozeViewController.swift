//
//  SnoozeViewController.swift
//  SportLife
//
//  Created by ludovic grimbert on 22/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import UIKit
import RxSwift
//import RxDataSources
import UserNotifications
import Action

class SnoozeViewController: UIViewController {
    
    @IBOutlet weak var snoozeTableView: UITableView!

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var sound1Button: UIButton!
    @IBOutlet weak var sound2Button: UIButton!
    @IBOutlet weak var everyWeekButton: UIButton!
    @IBOutlet weak var oneShotButton: UIButton!
    @IBOutlet weak var modeRepeatDayView: UIView!
    @IBOutlet weak var modeChoicedayView: UIView!
    @IBOutlet weak var sundayButton: UIButton!
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var thursdayButton: UIButton!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var soundLabel: UILabel!
    @IBOutlet weak var repeatLabel: UILabel!
    @IBOutlet weak var snoozesLabel: UILabel!
    

    var snoozeViewModel : SnoozeViewModel!
    
    var snoozeForDetail : Snooze!
    var info = Variable<String>("")

    let disposeBag = DisposeBag()
    
    func setupTableViewRx() {
        snoozeViewModel.notif
            .bind(to: snoozeTableView.rx.items(cellIdentifier: "SnoozeTableViewCell", cellType: SnoozeTableViewCell.self))
            {(_, item, cell) in
                cell.nameLabel.text = item.body
        }.disposed(by: disposeBag)
        
        
        snoozeTableView.rx.modelSelected(Snooze.self)
            .subscribe(onNext: {
            snooze in
                
                
                let alertController = UIAlertController(title: "main.deletethissnooze".translate, message: "main.areyousure".translate, preferredStyle: .alert)
                var ok = UIAlertAction.Action("Ok", style: .default)
                var cancel = UIAlertAction.Action("main.cancel".translate, style: .default )
                ok.rx.action = CocoaAction {
                    print("Alert's OK button was pressed")
                    self.snoozeViewModel.deleteNotif(uuid: snooze.uuid!)
                      self.info.value = "Notif List changing"
                    return .empty()
                }
                cancel.rx.action = CocoaAction {
                    print("Alert's Cancel button was pressed")
                    return .empty()
                }
                alertController.addAction(ok)
                alertController.addAction(cancel)
                self.present(alertController, animated: true, completion: nil)
                
                
           
        }).disposed(by: disposeBag)
        
        
        snoozeTableView.rx.itemSelected.subscribe(onNext: { [weak self]
            indexPath in
          // self?.performSegue(withIdentifier: "showDetailSnooze", sender: indexPath)
            self?.snoozeTableView.deselectRow(at: indexPath, animated: true)

        }).disposed(by: disposeBag)
 
        /*
        snoozeTableView.rx.itemDeleted
            .subscribe(onNext: { [unowned self] indexPath in
            })
            .disposed(by: disposeBag)
 */
        
    }
    
    func setupRx() {
        snoozeViewModel = SnoozeViewModel(info:self.info,localNotif: LocalNotif(), addItemTap: Observable.of(sundayButton.rx.tap,mondayButton.rx.tap,tuesdayButton.rx.tap,wednesdayButton.rx.tap,thursdayButton.rx.tap,fridayButton.rx.tap,saturdayButton.rx.tap).merge())

        datePicker.rx.value.asObservable()
            .subscribe(onNext: { date in
                self.snoozeViewModel.getDatePicker(date: date)
            })
            .disposed(by: disposeBag)
        
        // button sound
       sound1Button.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.sound.value = "alarme1.mp3"
                self.sound1Button.setTitleColor( #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), for: .normal)
                self.sound2Button.setTitleColor( #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1), for: .normal)
                self.modeRepeatDayView.isHidden = false
            })
            .disposed(by: disposeBag)
        
        sound2Button.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.sound.value = "alarme2.mp3"
               self.sound2Button.setTitleColor( #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), for: .normal)
                self.sound1Button.setTitleColor( #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1), for: .normal)
                self.modeRepeatDayView.isHidden = false
            })
            .disposed(by: disposeBag)
        
        // button mode repeat
        everyWeekButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.modeRepeat.value = true
                self.everyWeekButton.setTitleColor( #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), for: .normal)
                self.oneShotButton.setTitleColor( #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1), for: .normal)
                self.modeChoicedayView.isHidden = false

            })
            .disposed(by: disposeBag)
        
        oneShotButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.modeRepeat.value = false
                self.oneShotButton.setTitleColor( #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), for: .normal)
                self.everyWeekButton.setTitleColor( #colorLiteral(red: 0.09411764706, green: 0.2941176471, blue: 0.6588235294, alpha: 1), for: .normal)
                self.modeChoicedayView.isHidden = false

            })
            .disposed(by: disposeBag)
        
        // button week
        sundayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 1)
            })
            .disposed(by: disposeBag)
        mondayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 2)
            })
            .disposed(by: disposeBag)
        tuesdayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 3)
            })
            .disposed(by: disposeBag)
        wednesdayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 4)
            })
            .disposed(by: disposeBag)
        thursdayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 5)
            })
            .disposed(by: disposeBag)
        fridayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 6)
            })
            .disposed(by: disposeBag)
        saturdayButton.rx.tap.asObservable()
            .subscribe(onNext: { (_) in
                self.snoozeViewModel.scheduleSnooze(weekday: 7)
            })
            .disposed(by: disposeBag)
        
    }
    
    
    func updateDisplay() {
        datePicker.setValue(#colorLiteral(red: 0.7202371047, green: 0.09016391034, blue: 0.009056441441, alpha: 1) , forKey: "textColor")
        datePicker.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        modeRepeatDayView.isHidden = true
        modeChoicedayView.isHidden = true
        
        soundLabel.text = "main.sound".translate
        repeatLabel.text = "main.repeat".translate
        snoozesLabel.text = "main.snoozes".translate

        everyWeekButton.setTitle("main.everyweek".translate, for: .normal)
        oneShotButton.setTitle("main.oneshot".translate, for: .normal)
        sundayButton.setTitle("main.dimancheButton".translate, for: .normal)
        mondayButton.setTitle("main.lundiButton".translate, for: .normal)
        tuesdayButton.setTitle("main.mardiButton".translate, for: .normal)
        wednesdayButton.setTitle("main.mercrediButton".translate, for: .normal)
        thursdayButton.setTitle("main.jeudiButton".translate, for: .normal)
        fridayButton.setTitle("main.vendrediButton".translate, for: .normal)
        saturdayButton.setTitle("main.samediButton".translate, for: .normal)





    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayNavbar(title: "SNOOZE", imageBack: UIImage())
        updateDisplay()
        
        snoozeTableView.register(UINib(nibName: "SnoozeTableViewCell", bundle: nil), forCellReuseIdentifier: "SnoozeTableViewCell")

        setupRx()
       setupTableViewRx()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
*/
