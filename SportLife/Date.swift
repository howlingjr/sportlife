//
//  Date.swift
//  SportLife
//
//  Created by ludovic grimbert on 04/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//

import Foundation

extension Date {
    
    func fetchYear() {
        let date = Date()
        let dateYesterday = date.addingTimeInterval(-24 * 60 * 60)
        let myDateYesterdayTotal = dateYesterday.absoluteDateToString
        let test = myDateYesterdayTotal.substring(from: 4)
        guard let testToInt = Int(test) else {return}
        if testToInt > 1 && testToInt < 800 {
            var myDateInt = Int(dateYesterday.absoluteDateToString2)
            myDateInt = myDateInt! - 1
            year = "\(myDateInt!)"
            StandingSettings.sharedInstance.registerModeApp(newAppModeSelected: year)
        }else {
            year = dateYesterday.absoluteDateToString2
            StandingSettings.sharedInstance.registerModeApp(newAppModeSelected: year)
        }
        
    }
    
}
