//
//  MatchViewModel.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation


import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import ObjectMapper


struct MatchViewModel {
    
    lazy var rx_matchs: Driver<[Match]> = self.fetchMatchs()
    
    var date: Observable<String?>
    
    init(withNameObservable nameObservable: Observable<String?>) {
        self.date = nameObservable
    }
    
    
    private func fetchMatchs() -> Driver<[Match]> {
        print("fetchMatchs")
                  return date
            .subscribeOn(MainScheduler.instance)
            .do(onNext: { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            })
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { date in
                return RxAlamofire
                    .requestJSON(.get, "http://data.nba.com/5s/json/cms/noseason/scoreboard/\(date!)/games.json")
                   //.share(replay: 1)
                    //.retry(3)
                    //.debug()
                    .catchError { error in
                        return Observable.never()
                }
            }
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            //.throttle(0.3, scheduler: MainScheduler.instance)
            .map { (response, json) -> [Match] in
                 if let result = Mapper<MatchResponse>().map(JSONObject: json) {
                    return result.game!
                 } else {
                 return []
                 }
            }
            .observeOn(MainScheduler.instance)
            .do(onNext: { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
    
            .asDriver(onErrorJustReturn: []) // This also makes sure that we are on MainScheduler
        
        
    
        
    }

    
    
}
*/
