//
//  PlayoffViewModel.swift
//  SportLife
//
//  Created by ludovic grimbert on 05/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation

import RxSwift
import Alamofire


class PlayoffViewModel {
    
    fileprivate let playoffService: PlayoffService
    
    //fileprivate let disposeBag = DisposeBag()
    let playoffVariable = Variable<[Playoff]>([Playoff]())
    let playoff: Observable<[Playoff]>
   
    
    
    init(playoffService: PlayoffService) {
        self.playoffService = playoffService
        
        // tester avec flatMap à la place de flatMapLatest
        playoff = playoffVariable.asObservable()
            .flatMapLatest { _ -> Observable<[Playoff]> in
                if NetworkReachabilityManager()!.isReachable == true {
                    return playoffService.fetchPlayoff(StandingSettings.sharedInstance.getModeApp())
                   // return playoffService.fetchPlayoff("2016")
                }
                return Observable.just([Playoff]())
        }
        
        
       
        
    }
    
    
}
*/
