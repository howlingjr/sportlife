//
//  UserDefaultsManager.swift
//  SportLife
//
//  Created by ludovic grimbert on 04/01/2018.
//  Copyright © 2018 ludovic grimbert. All rights reserved.
//
/*
import Foundation



class StandingSettings: NSObject {
    
    // MARK: - Properties
    static let sharedInstance = StandingSettings()
    private var mode: String = "2016"
    
    // MARK: - Private
    private override init() {
        
        if (UserDefaults.standard.object(forKey: "Year") == nil) {
            UserDefaults.standard.set("2016", forKey: "Year")
        } else {
            mode =  UserDefaults.standard.string(forKey: "Year")!
        }
    }
    
    // MARK: - Public
    // MARK: - Register
    /**
     * Register mode App selected by the user
     * Parameters:
     *  - newAppModeSelected: app mode
     */
    func registerModeApp(newAppModeSelected: String!) {
        
        mode = newAppModeSelected
        UserDefaults.standard.set(newAppModeSelected, forKey: "Year")
    }
    
    // MARK: - Get
    /**
     * Get mode App
     * Returns:
     *  - AppMode: return current mode App selected
     */
    func getModeApp() -> String! {
        
        return mode
    }
}
*/
